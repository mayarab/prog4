package app;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) throws Exception {

    	System.out.println("Informe a montadora:");
    	String montadora;
    	Scanner scanner = new Scanner(System.in);
    	montadora = scanner.nextLine();
    	Chassi c1 = null;
    	Motor m1;
    	
    	switch(montadora)
    	{
	    	case "toyota":
	    		c1 = new ToyotaChassi();
	    		m1 = new ToyotaMotor();
	            c1.setMotor(m1);
	    		break;
	    		
	    	case "volks":
	    		c1 = new VolkswagenChassi();
	    		m1 = new VolkswagenMotor();
	            c1.setMotor(m1);	    		
	    		break;
	    		
	    	default:
	    		throw new RuntimeException("Montadora Inv�lida");
    	}
    	
        c1.getMotor().igni��o();
        
    }

}

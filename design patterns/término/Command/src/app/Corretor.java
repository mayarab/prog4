package app;

import java.util.ArrayList;
import java.util.List;

public class Corretor {
	private List<Comando> ordens = new ArrayList<>();
	
	public void colocarUmaOrdem(Comando ordem){
		this.ordens.add(ordem);
	}
	
	public void processarOrdens(){
		for(Comando ordem : ordens){
			ordem.executar();
		}
	}
}

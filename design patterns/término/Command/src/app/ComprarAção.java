package app;

public class ComprarA��o implements Comando {

	private A��o a��o;

	public ComprarA��o(A��o a��o){
		this.a��o = a��o;
	}
	
	@Override
	public void executar() {
		a��o.comprar();
	}
}

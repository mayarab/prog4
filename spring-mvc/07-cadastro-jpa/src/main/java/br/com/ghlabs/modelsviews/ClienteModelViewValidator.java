package br.com.ghlabs.modelsviews;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.ghlabs.models.Cliente;

public class ClienteModelViewValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// http://docs.oracle.com/javase/6/docs/api/java/lang/Class.html#isAssignableFrom(java.lang.Class)
		return ClienteModelView.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ClienteModelView clienteModelView;
		clienteModelView = (ClienteModelView) target;
		
		Cliente cliente;
		cliente = clienteModelView.getCliente();
		
		if( ! clienteModelView.getConfirmacaoSenha().equals(cliente.getSenha())){
			errors.rejectValue("confirmacaoSenha", "", "A senha e a confirmação não são iguais");
		}		
	}

}

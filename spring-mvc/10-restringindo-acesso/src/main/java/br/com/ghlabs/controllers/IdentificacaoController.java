package br.com.ghlabs.controllers;



import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IdentificacaoController {
	
//	@RequestMapping("/logout")
//	public String sair(){
//		return "logout";
//	}

	@RequestMapping("/identificar")
	public String identificar(){
		return "usuario-identificar";
	}
	
	@RequestMapping("/usuario")
	public String formulario(){
		return "usuario-formulario";
	}
	
	@RequestMapping(value="/usuario",
					method=RequestMethod.POST)
	public String tratarFormulario(@RequestParam String nome,
								   HttpServletResponse response){	
		
		response.addCookie(new Cookie("nome", nome));
		return "usuario-formulario-sucesso";
	}
	

	
}

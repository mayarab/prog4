package br.com.ghlabs.modelsviews;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import br.com.ghlabs.models.Produto;

public class CarrinhoCompra {
	
	private List<CarrinhoCompraItem> items;
	
	public CarrinhoCompra(){
		items = new ArrayList<CarrinhoCompraItem>();
	}
	
	public void adicionar(Produto produto, int quantidade){
		
		CarrinhoCompraItem item;
		item = new CarrinhoCompraItem();
		item.setProduto(produto);
		item.setQuantidade(quantidade);
		items.add(item);
	}
	
	public BigDecimal getTotal()
	{
		BigDecimal total;
		total = new BigDecimal(0);
		items.stream().forEach(i -> {
			total.add(i.getProduto().getPreco());
		});
		
		return total;
	}

	public List<CarrinhoCompraItem> getItems() {
		return items;
	}

	public void setItems(List<CarrinhoCompraItem> items) {
		this.items = items;
	}
}

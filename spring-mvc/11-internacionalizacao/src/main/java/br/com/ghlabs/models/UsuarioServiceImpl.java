package br.com.ghlabs.models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioServiceImpl 
	implements UserDetailsService
			 , UsuarioService{

	@PersistenceContext(unitName="lojaPU")
	private EntityManager entityManager;
	
	@Override
	public UserDetails loadUserByUsername(String username) 
			throws UsernameNotFoundException {

		String jpql;
		jpql = "select u from Usuario u where u.username = :username";
		List<Usuario> usuarios;
		usuarios = entityManager.createQuery(jpql, Usuario.class)
								.setParameter("username", username)
								.getResultList();
		
		if( usuarios.isEmpty() ) {
			throw new UsernameNotFoundException("Usuario nao existe");
		}
		
		return usuarios.get(0);
	}

	@Override
	@Transactional
	public void cadastrarUsuario(Usuario usuario) {
		
		BCryptPasswordEncoder encoder;
		encoder = new BCryptPasswordEncoder();
		
		String passwordCriptografado;
		passwordCriptografado = encoder.encode(usuario.getPassword());
		usuario.setPassword(passwordCriptografado);
		
		entityManager.persist(usuario);
	}
}

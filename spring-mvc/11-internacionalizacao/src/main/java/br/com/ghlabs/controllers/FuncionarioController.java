package br.com.ghlabs.controllers;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.Funcionario;

@Controller
public class FuncionarioController {

	@RequestMapping("/funcionario/novo")
	public String novo(Model model)
	{
		model.addAttribute("funcionario", 
							new Funcionario());
		return "funcionario-novo";
	}
	
	@RequestMapping(value="/funcionario/novo",
					method=RequestMethod.POST)
	public String novo(@Valid 
					   Funcionario funcionario,
					   BindingResult result) {
		

		boolean e = result.hasErrors();
		
		return "funcionario-novo";
	}	
	
}

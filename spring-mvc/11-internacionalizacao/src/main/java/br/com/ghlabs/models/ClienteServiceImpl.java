package br.com.ghlabs.models;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl 
       implements ClienteService{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private UsuarioService usuarioService;

	@Override
	public void cadastrar(Cliente c) {
		usuarioService.cadastrarUsuario(c.getUsuario());
		clienteRepository.inserir(c);
	}
}
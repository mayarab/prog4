package br.com.ghlabs.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ghlabs.models.Produto;
import br.com.ghlabs.models.ProdutoService;

@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping("/produtos/novo")
	public String novo(){
		return "produto-novo";
	}
	
	
	@RequestMapping("/produtos")
	public String todos(@RequestParam(value="categoriaId", 
									  required= false) 
						Long id,
						Model model)
	{
		if( id == null )
			model.addAttribute("produtos", 
					produtoService.obterTodos());			
		else
			model.addAttribute("produtos", 
					produtoService.obterPorCategoria(id));
		

		return "produtos";
	}
	
	
	@RequestMapping("/detalhe/{id}")
	public String detalhe(@PathVariable("id")long id,
						  Model model){
		Produto produto;
		produto = produtoService.obterPorId(id);
		model.addAttribute("produto", produto);
		return "produto-detalhe";
	}
	
	@RequestMapping(value="/produtos/novo", method=RequestMethod.POST)
	public String detalhe(@RequestParam(name="preco", required=false) 
						  @NumberFormat(style= Style.NUMBER)
						  BigDecimal preco ){
		return "produto-novo";
	}
}

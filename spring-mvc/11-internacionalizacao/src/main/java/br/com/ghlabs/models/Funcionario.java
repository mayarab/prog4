package br.com.ghlabs.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.stereotype.Component;

@Component
public class Funcionario implements MessageSourceAware  {
	
	private MessageSource messageSource;
	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getMessage(String codigo){
		return messageSource.getMessage(codigo,
				new Object[0], 
				Locale.getDefault());
	}
	//@DateTimeFormat(pattern="MM/dd/yyyy")
	//@DateTimeFormat(pattern="${data.formato}")
	//@DateTimeFormat(pattern="${data.formato}")
	//@DateTimeFormat(pattern="#{messageSource.getMessage('data.formato')}")
	//@DateTimeFormat(pattern="${messageSource.getMessage('data.formato')}")
	//@DateTimeFormat(style="S-")
	//@DateTimeFormat(pattern="${teste}")
	@DateTimeFormat
	@NotNull
	private Date dataNascimento;
	@NumberFormat(style= Style.NUMBER)
	@Range(min=0, message="Salário deve ser maior que zero" )
	private BigDecimal salario;
	
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public BigDecimal getSalario() {
		return salario;
	}
	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

}

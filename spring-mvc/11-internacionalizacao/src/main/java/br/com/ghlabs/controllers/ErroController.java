package br.com.ghlabs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

// https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc

@Controller
public class ErroController {

	@RequestMapping("/acesso-negado")
	public String acessoNegado(){
		return "acesso-negado";
	}
	
}

<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Cadastro de Funcionários</h1>
<form:form modelAttribute="funcionario">
	<label>Data Nascimento:</label>
	<form:input path="dataNascimento" />
	<form:errors path="dataNascimento" />
	<label>Salário:</label>
	<form:input path="salario" />
	<form:errors path="salario" />
	<input type="submit" value="Salvar" />
</form:form>

</body>
</html>
<%@ taglib prefix="form" 
		   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tags" 
		   uri="http://www.springframework.org/tags" %>
		   
		   
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LOJA - CADASTRO DO CLIENTE</title>
<style>
	label, input[type=submit]{
		display:block;
	}
</style>
</head>
<body>
<form:errors path="*" />
<form:form modelAttribute="clientemv">
	<form:label path="cliente.nome">
		<tags:message code="cliente.novo.nome" />
	</form:label>
	<form:input path="cliente.nome" />
	<form:errors path="cliente.nome"/>

	<form:label path="cliente.usuario.username">
		<tags:message code="cliente.novo.username" />
	</form:label>
	<form:input path="cliente.usuario.username" />
	<form:errors path="cliente.usuario.username"/>

	<form:label path="cliente.cpf">
		<tags:message code="cliente.novo.cpf" />
	</form:label>
	<form:input path="cliente.cpf" />
	<form:errors path="cliente.cpf"/>
	
	<form:label path="cliente.dataNascimento">
		<tags:message code="cliente.novo.dataNascimento" />
	</form:label>
	<form:input path="cliente.dataNascimento" type="date" />
	<form:errors path="cliente.dataNascimento"/>
		
	<form:label path="cliente.sexo">
		<tags:message code="cliente.novo.sexo" />	
	</form:label>
	<form:select path="cliente.sexo">
		<form:option value="M">
			<tags:message code="cliente.novo.sexo.masculino" />
		</form:option>
		<form:option value="F">
			<tags:message code="cliente.novo.sexo.feminino" />
		</form:option>
	</form:select>
	<form:errors path="cliente.sexo"/>
	
	<form:label path="cliente.usuario.password">
		<tags:message code="cliente.novo.password" />	
	</form:label>
	<form:password path="cliente.usuario.password" />
	<form:errors path="cliente.usuario.password"/>
	
	<form:label path="confirmacaoSenha">
		<tags:message code="cliente.novo.confirmacaoSenha" />
	</form:label>
	<form:password path="confirmacaoSenha" />
	<form:errors path="confirmacaoSenha"/>
	
	<form:label path="cliente.receberOfertaPorEmail">
		<tags:message code="cliente.novo.receberOfertaPorEmail" />
	</form:label>
	
	<form:checkbox path="cliente.receberOfertaPorEmail" 
				   value="cliente.receberOfertaPorEmail" />
	<form:errors path="cliente.receberOfertaPorEmail"/>
	
	
	<input type="submit" value="salvar" />
	
	<p><a href="novo?lang=pt_BR">Portugu�s Brasil</a></p>
	<p><a href="novo?lang=en_US">Ingl�s</a></p>
	
</form:form>
</body>
</html>
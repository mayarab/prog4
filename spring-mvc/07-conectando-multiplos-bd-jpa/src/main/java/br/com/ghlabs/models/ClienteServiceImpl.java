package br.com.ghlabs.models;


import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghlabs.financeiro.models.FinanceiroService;
import br.com.ghlabs.financeiro.models.Lancamento;

@Service
public class ClienteServiceImpl 
       implements ClienteService{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private FinanceiroService financeiroService;
	
	@Override
	public void cadastrar(Cliente cliente) {
		clienteRepository.inserir(cliente);
		Lancamento lancamento;
		lancamento = new Lancamento();
		lancamento.setDescricao("VALE BRINDE - " + cliente.getNome() );
		lancamento.setTipo(Lancamento.Tipo.SAIDA);
		lancamento.setDataLancamento(new Date());
		lancamento.setValor( new BigDecimal(100.0));
		financeiroService.registrarLancamento(lancamento);
	}
	
}
package br.com.ghlabs.models;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




@Primary
@Repository
public class JpaClienteRepository implements ClienteRepository{

	@PersistenceContext(unitName="SitePU")
	private EntityManager entityManager;

	@Transactional("siteTransactionManagerBean")
	@Override
	public void inserir(Cliente cliente) {
		entityManager.persist(cliente);
	}
}

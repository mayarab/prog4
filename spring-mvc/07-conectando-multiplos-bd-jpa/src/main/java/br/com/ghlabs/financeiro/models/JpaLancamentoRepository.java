package br.com.ghlabs.financeiro.models;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class JpaLancamentoRepository 
implements LancamentoRepository{

	@PersistenceContext(unitName="FinanceiroPU")
	private EntityManager entityManager;
	
	@Transactional("financeiroTransactionManagerBean")
	@Override
	public void salvar(Lancamento lancamento) {
		entityManager.persist(lancamento);
	}
}

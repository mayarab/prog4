package br.com.ghlabs;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;



@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix="datasource.site")
	public DataSource siteDataSourceBean(){
		return DataSourceBuilder.create()
								.build();
	}

	@Primary
	@Bean(name="siteEntityManagerFactoryBean")
	public LocalContainerEntityManagerFactoryBean 
		   siteEntityManagerFactoryBean(EntityManagerFactoryBuilder builder){
		return builder.dataSource(siteDataSourceBean())
					  .packages("br.com.ghlabs.models")
					  .persistenceUnit("SitePU")
					  .build();
	}
	
	@Primary
	@Bean (name="siteTransactionManagerBean")
	public JpaTransactionManager siteTransactionManagerBean(
				@Qualifier("siteEntityManagerFactoryBean") 
				EntityManagerFactory emf) {
	    JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(emf);
	    return transactionManager;
	}
	
	
	@Bean
	@ConfigurationProperties(prefix="datasource.financeiro")
	public DataSource financeiroDataSourceBean(){
		return DataSourceBuilder.create()
								.build();
	}	

	@Bean(name="financeiroEntityManagerFactoryBean")
	public LocalContainerEntityManagerFactoryBean 
		   financeiroEntityManagerFactoryBean(EntityManagerFactoryBuilder builder){
		return builder.dataSource(financeiroDataSourceBean())
					  .packages("br.com.ghlabs.financeiro.models")
					  .persistenceUnit("FinanceiroPU")
					  .build();
	}
	
	@Bean(name="financeiroTransactionManagerBean")
	public JpaTransactionManager financeiroTransactionManagerBean(
				   @Qualifier("financeiroEntityManagerFactoryBean") 
				   EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(emf);
	    
	    return transactionManager;
	}
	
}

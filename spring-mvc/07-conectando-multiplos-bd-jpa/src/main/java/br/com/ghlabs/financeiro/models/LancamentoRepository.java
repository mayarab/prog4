package br.com.ghlabs.financeiro.models;

public interface LancamentoRepository {
	void salvar(Lancamento lancamento);
}

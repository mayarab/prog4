package br.com.ghlabs.financeiro.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinanceiroServiceImpl 
	implements FinanceiroService{

	@Autowired 
	private LancamentoRepository lancamentoRepository;

	@Override
	public void registrarLancamento(Lancamento lancamento) {
	
		lancamentoRepository.salvar(lancamento);
	}
}

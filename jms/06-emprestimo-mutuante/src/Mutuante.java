import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

public class Mutuante implements MessageListener{
	private QueueSession sessao;
	private QueueReceiver recebedor;

	public Mutuante()
	{
		 try
	        {
	            InitialContext contexto;
	            contexto = new InitialContext();

	            ConnectionFactory fabrica;
	            fabrica = (ConnectionFactory) contexto.lookup("EmprestimosCF");
	            
	            Connection conexao;
	            conexao = fabrica.createConnection();
	            
	            sessao = (QueueSession) conexao.createSession(false, Session.AUTO_ACKNOWLEDGE);
	            
	            Queue requisicoesEmprestimo;
	            requisicoesEmprestimo = sessao.createQueue("FilaRequisicoesEmprestimo");
	            
	            recebedor = sessao.createReceiver(requisicoesEmprestimo);
	            recebedor.setMessageListener(this);
	            
	            conexao.start();
	        }
	        catch(Exception ex )
	        {
	            throw new RuntimeException(ex);
	        }   
	}

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		
	}
}

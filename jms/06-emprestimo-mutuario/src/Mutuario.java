import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

public class Mutuario 
	   implements MessageListener
{
	
	private Queue requisicoesEmprestimo;
	private Queue requisicoesEmprestimoRespostas;
	private QueueSession sessao;
	private QueueSender enviador;
	private QueueReceiver recebedor;

	public Mutuario()
	{
		try
		{
			InitialContext contexto;
            contexto = new InitialContext();

            ConnectionFactory fabrica;
            fabrica = (ConnectionFactory) contexto.lookup("EmprestimosCF");
            
            Connection conexao;
            conexao = fabrica.createConnection();       
            sessao = (QueueSession) conexao.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            requisicoesEmprestimo = sessao.createQueue("FilaRequisicoesEmprestimo");
            requisicoesEmprestimoRespostas = sessao.createQueue("FilaRequisicoesEmprestimoRespostas");

            enviador = sessao.createSender(requisicoesEmprestimo);
            recebedor = sessao.createReceiver(requisicoesEmprestimoRespostas);
            
            recebedor.setMessageListener(this);
            
            conexao.start();
            
		}
		catch( Exception ex )
		{
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		
	}

	public void solicitar(float salario, float emprestimo) 
			throws JMSException {
	   MapMessage mensagem;
	   mensagem = sessao.createMapMessage();
	   mensagem.setDouble("salario", salario);
	   mensagem.setDouble("emprestimo", emprestimo);
	   enviador.send(mensagem);		
	}
}

import java.util.Scanner;

import javax.jms.JMSException;

public class Program {
	public static void main(String args[]) throws JMSException{
		Mutuario mutuario;
		mutuario = new Mutuario();
		
		Scanner scanner = new Scanner(System.in);
		float salario;
		float emprestimo;
		do
		{
			System.out.println("Informe o salario:");
			salario = scanner.nextFloat();
			
			System.out.println("Informe o empréstimo:");
			emprestimo = scanner.nextFloat();
			
			mutuario.solicitar(salario, emprestimo);
			System.out.println("Empréstimo foi solicitado");
		}
		while(true);
	}
}
